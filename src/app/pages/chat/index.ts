import { Routes } from '@angular/router';

export const routes: Routes = [
    {
        path: 'chat',
        title: 'Chat',
        loadComponent: async () => (await import('./chat.component')).ChatComponent,
    },
];
