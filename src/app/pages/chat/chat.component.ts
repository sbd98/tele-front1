import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@lib/services';

@Component({
    standalone: true,
    imports: [CommonModule],
    templateUrl: './chat.component.html',
})
export class ChatComponent {

    private readonly _router = inject(Router);
    private readonly _authService = inject(AuthService);

    chat(): void {
        this._authService.chat();

        this._router.navigate(['/chat']);
    }
}
