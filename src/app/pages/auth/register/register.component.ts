import { CommonModule } from '@angular/common';
import { Component,inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '@lib/services';
@Component({
    standalone: true,
    imports: [CommonModule],
    templateUrl: './register.component.html',
})
export class RegisterComponent {
    private readonly _router = inject(Router);
    private readonly _authService = inject(AuthService);

    login(): void {
        this._authService.login();

        this._router.navigate([]);
    }
    back(): void {
        this._authService.back();

        this._router.navigate(['/auth/login']);
    }
    
}
